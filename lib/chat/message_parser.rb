# frozen_string_literal: true

require 'uri'
require './lib/config'
require './lib/holiday_checker'
require './lib/chat/members'
require './lib/chat/ping_forecaster'

module Chat
  class MessageParser
    include SemanticLogger::Loggable

    SPREADSHEET_URL = 'https://tinyurl.com/wtdao82'

    def initialize(event, data)
      @event = event
      @data = data
      @message = data['text']&.split&.last&.strip || ''
      @user = data['user'] || data['user_id']
      @ts = data['ts']
      @command = data['command']
    end

    def parse
      if @message == 'position'
        PingForecaster.new(@user).position
      elsif @message == 'top'
        PingForecaster.new(@user).top
      elsif @message == 'test-reaction' && !Config.slack_output
        Thread.new { Chat::ReactionResponder.new(@event, @data).respond }
      elsif HolidayChecker.new.today?
        respond_to_holiday
      elsif @command == Config.command
        issue_url ? respond_to_incident : invalid_incident
      end
    end

    private

    def respond_to_holiday
      logger.info('Weekend/holiday triggered - ignoring', reported_by: client.get_username(@user))

      client.message("Please use the spreadsheet for weekend or holiday escalations: #{SPREADSHEET_URL})")
    end

    def respond_to_incident
      logger.info('New incident', issue_url: issue_url, reported_by: client.get_username(@user))

      unless Config.slack_output
        client.pm_message('Sorry, @pagerslack is currently in maintenance mode. Please wait a few seconds and try again.', user: @user, force: true)
      end

      Chat::Members.new(@event, issue_url, @user).notify_online_member
    end

    def invalid_incident
      logger.debug('Invalid incident (no URL)', reported_by: client.get_username(@user), message: @message)

      client.pm_message("<@#{@user}> please pass an incident URL: `#{Config.command} https://incident.url`", user: @user)
    end

    def issue_url
      @issue_url ||= URI.extract(@message)&.first
    end

    def client
      Chat::Client.instance
    end
  end
end
