# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'
require './lib/chat/ping_forecaster'
require 'slack'

describe Chat::PingForecaster do
  let(:ts) { Time.new(2020).to_f }
  subject(:forecaster) { described_class.new('member') }

  describe '#position' do
    before do
      allow_any_instance_of(Chat::Client).to receive(:get_username).and_return('member')
      allow(forecaster).to receive(:refresh_member_list)
      allow(forecaster).to receive(:oncall_list).with(any_args).and_return({ 'member' => [{ 'name': 'member', 'slack': 'member', weekend: 'No' }] })
    end

    context 'no member' do
      it 'sends the right message' do
        expect(Chat::Client.instance).to receive(:pm_message).with("<@member> Sorry, I can't seem to find you in the oncall list", user: 'member')

        forecaster.position
      end
    end

    context 'member exists' do
      it 'sends a message if the member exists' do
        expect(Chat::Client.instance).to receive(:pm_message).with("<@member> Sorry, I can't seem to find you in the oncall list", user: 'member')

        forecaster.position
      end
    end
  end
end
