# frozen_string_literal: true

require 'spec_helper'
require 'semantic_logger'
require 'timecop'

require './lib/holiday_checker'

describe HolidayChecker do
  before do
    stub_const('HolidayChecker::HOLIDAYS_FILENAME', './holidays.yml.example')
  end

  subject(:holiday) { described_class.new }

  it 'returns true on weekend' do
    Timecop.freeze(Time.find_zone("UTC").parse('2020-11-22')) do
      expect(subject.today?).to be true
    end
  end

  it 'returns true on holiday' do
    Timecop.freeze(Time.find_zone("UTC").parse('2020-12-25')) do
      expect(subject.today?).to be true
    end
  end

  it 'returns false on a working day' do
    Timecop.freeze(Time.find_zone("UTC").parse('2020-11-23')) do
      expect(subject.today?).to be false
    end
  end
end
